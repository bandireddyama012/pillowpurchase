package TestNGListenerConcept;

import org.testng.ITestNGListener;
import org.testng.ITestResult;

import com.Midtrans.PillowPurchase.base.TestBase;
/*
 * This Class is Listener class basically used to Take screen shot on any Test Failure.
 */
public class CustomListner extends TestBase implements ITestNGListener {

	public void onTestStart(ITestResult result){
		
	}
	
   public void onTestSuccesss(ITestResult result){
		
}
  //This Listener Takes screen shot when any Test fails. 
   public void onTestFailure(ITestResult result){
	   
	   System.out.println("FAILED TEST");
	   failed(result.getMethod().getMethodName());//failed method in base
		
	}
	
}
