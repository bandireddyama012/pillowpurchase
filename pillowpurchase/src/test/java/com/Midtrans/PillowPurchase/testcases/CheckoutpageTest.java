package com.Midtrans.PillowPurchase.testcases;

import org.openqa.selenium.By;
/*
 * Actual Test cases as per requirement.
 */
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Listeners;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.Midtrans.PillowPurchase.base.TestBase;
import com.Midtrans.PillowPurchase.pages.Checkoutpage;

import TestNGListenerConcept.CustomListner;


@Listeners(CustomListner.class) //this Listener will capture screen shot on any Test Failure .
public class CheckoutpageTest extends TestBase {
	Checkoutpage chkoutpage;
	
	public CheckoutpageTest(){
		super();// This calls Testbase class constructor so that config property is loaded
	}
	
	@BeforeClass
	@Parameters("browser")
	public void setup(String browser){
		initialization(browser);
		chkoutpage = new Checkoutpage();//object of checkoutpage class
		}
	
	@AfterClass
	public void tearDown(){
		driver.quit();
	}
	
	/*
	 * Verifying Title of the given website
	 */
	@Test(priority=1)
	
	public void checkoutpagetitleTest(){
		String title = chkoutpage.ValidateHomepagetitile();
		 Assert.assertEquals(title, "Sample Store");
		
	}
	
	/*
	 * Verifying Valid Transaction status
	 */
	@Test(priority=2)
	
	public void validtransactionsuccess() throws InterruptedException{
		chkoutpage.Checkout(prop.getProperty("creditcarnumber"), prop.getProperty("expdate"), prop.getProperty("cvv"), prop.getProperty("otp"));
		Assert.assertTrue(chkoutpage.Transactionsuccess());
	}
	
	/*
	 * Verifying invalid Transaction status
	 */
	@Test(priority=3)
	public void invalidtransactionFailure() throws InterruptedException{
		chkoutpage.Checkout(prop.getProperty("creditcarnumber"), prop.getProperty("expdate"), prop.getProperty("cvv"), prop.getProperty("wrongotp"));
		driver.switchTo().frame(driver.findElement(By.xpath("//iframe[@id='snap-midtrans']")));
		Assert.assertTrue(chkoutpage.Transactionstatus());
		Assert.assertTrue(chkoutpage.Transactiondecliendmessage());
		
	}
	
	

}
