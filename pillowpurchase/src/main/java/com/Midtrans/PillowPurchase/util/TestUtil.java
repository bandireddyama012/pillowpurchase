package com.Midtrans.PillowPurchase.util;

/*
 * This class is util class which as various utilities which is parameterized to the script.
 */

//we create time out and implicit wait in order to facilitate time when app loads slow
public class TestUtil {

	
	
	public static long PAGE_LOAD_TIMEOUT = 20;
	public static long IMPLICIT_WAIT = 10;
}
