package com.Midtrans.PillowPurchase.pages;


import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.Midtrans.PillowPurchase.base.TestBase;



/*
 * Total End to End Check out Page Operation class
 * 
 */
public class Checkoutpage extends TestBase {
	
	//page Factory - OR:
	
	@FindBy(xpath="//div[@class='title' and text()='Midtrans Pillow']")
	WebElement pillowHome;
	
	@FindBy(xpath="//a[text()='BUY NOW']")
	WebElement BuyBtn;
	
	@FindBy(xpath="//div[@class='cart-checkout' and text()='CHECKOUT']")
	WebElement CheckoutBtn;
	
	@FindBy(xpath="//*[@id='application']/div[1]/a")
	WebElement continueBtn;
	
	@FindBy(xpath="//div[@class='list-title text-actionable-bold' and text()='Credit Card']")
	WebElement CreditcardBtn;
	
	@FindBy(xpath="//input[@type='tel' and @name='cardnumber']")
	WebElement CreditcardText;
	
	@FindBy(xpath="//input[@placeholder='MM / YY']")
	WebElement CreditEXPText;
	
	@FindBy(xpath="//input[@placeholder='123']")
	WebElement CreditcvvText;
	
	@FindBy(xpath="//input[@name='promo']")
	WebElement PromoChcekBox;
	
	@FindBy(xpath="//a[@class='button-main-content']")
	WebElement PayNowBtn;
	
	@FindBy(xpath="//input[@id='PaRes']")
	WebElement OtpText;
	
	@FindBy(xpath="//button[@type='submit']")
	WebElement submitBtn;
	
	@FindBy(xpath="//span[text()='Thank you for your purchase.']")
	WebElement Purchaseconformlogo;
	
	@FindBy(xpath="//div[@class='text-failed text-bold']")
    WebElement Transactionfailedimg;
			
	@FindBy(xpath="//span[text()='Your card got declined by the bank']")
	WebElement Transactiondeclinedimg;
	
	//creating constructor to initialize Pagefactory page objects
	public Checkoutpage()
	{
		PageFactory.initElements(driver, this); //this pointing to the current class object
	}
	
	//Actions on the Page
	
	public String ValidateHomepagetitile(){
		return driver.getTitle();
	}
	
	// checking image is visible or not in Home Page
	
	public boolean validatepillowimage(){
		return pillowHome.isDisplayed();
	}
	
	/*
	 * Actual check out Operation
	 */
	public void Checkout(String cn, String exp, String cvv, String otp ) throws InterruptedException{
		BuyBtn.click();
		CheckoutBtn.click();
		Thread.sleep(5000);
		//Switching to iframe
		driver.switchTo().frame(driver.findElement(By.xpath("//iframe[@id='snap-midtrans']")));
		continueBtn.click();
		CreditcardBtn.click();
		CreditcardText.clear();
		CreditcardText.sendKeys(cn);
		CreditEXPText.clear();
		CreditEXPText.sendKeys(exp);
		CreditcvvText.clear();
		CreditcvvText.sendKeys(cvv);
	    PromoChcekBox.click();
		PayNowBtn.click();
		//switching to iframe
		driver.switchTo().frame(driver.findElement(By.xpath("//*[@id='application']/div[3]/div/div/div/iframe")));
		OtpText.clear();
		OtpText.sendKeys(otp);
		submitBtn.click();
		//switching to default i frame
		driver.switchTo().defaultContent();
	}
	
	/*
	 * checking Transaction status for success
	 */
	public boolean Transactionsuccess(){
		
		return Purchaseconformlogo.isDisplayed();
	}
	
	/*
	 * checking Transaction status for failure
	 */
	public boolean Transactionstatus(){
		return Transactionfailedimg.isDisplayed();
	}
	
	/*
	 * checking Transaction status Message
	 */
	public boolean Transactiondecliendmessage(){
		return Transactiondeclinedimg.isDisplayed();
	}
	
}


