package com.Midtrans.PillowPurchase.base;
/*
 * This Class is actual Test Base class which is super class to all the classes in this Project.
 */
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import com.Midtrans.PillowPurchase.util.TestUtil;

public class TestBase {
	
	public static WebDriver driver;
	public static Properties prop;
	
	//constructor for class
	public TestBase(){
		
		//loading Properties File
		try{
			prop = new Properties();
			FileInputStream ip = new FileInputStream("C:/Users/sravan/Desktop/purchase pillow/pillowpurchase/pillowpurchase/src/main/java/com/Midtrans/PillowPurchase/config/config.properties");
			prop.load(ip);
		}catch (FileNotFoundException e){
			e.printStackTrace();
		}catch (IOException e){
			e.printStackTrace();
		}
		
	}
	

// We are initializing browser deciding factor and implicit waits and Url

public static void initialization(String browserName){
	//String browserName = prop.getProperty("browser");
	if(browserName.equalsIgnoreCase("chrome")){
		System.setProperty("webdriver.chrome.driver", "C:/Users/sravan/Desktop/purchase pillow/pillowpurchase/pillowpurchase/Drivers/chromedriver.exe");
      driver = new ChromeDriver();
		System.out.println("Test Test Test");
	}
	
	
	else if(browserName.equalsIgnoreCase("firefox")){
		System.setProperty("webdriver.gecko.driver", "C:/Users/sravan/Desktop/purchase pillow/pillowpurchase/pillowpurchase/Drivers/geckodriver.exe");
		driver = new FirefoxDriver();
	}
	
	driver.manage().window().maximize();
	driver.manage().deleteAllCookies();
	driver.manage().timeouts().pageLoadTimeout(TestUtil.PAGE_LOAD_TIMEOUT, TimeUnit.SECONDS);
	driver.manage().timeouts().implicitlyWait(TestUtil.IMPLICIT_WAIT, TimeUnit.SECONDS);
	
	//initializing URL
	driver.get(prop.getProperty("url"));
	
}

   public void failed(String testMethodName){
	File srcFile =((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
	try{
		//every time it gives new screen shot with file name unique
	FileUtils.copyFile(srcFile,  new File("C:/Users/sravan/Desktop/purchase pillow/pillowpurchase/pillowpurchase/screenshots/"+ testMethodName +  "_"+".jpg"));
	}catch (IOException e){
		e.printStackTrace();
	}
	}
}
